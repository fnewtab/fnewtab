# FNewTab

Server-configurable new tab page.

## Build instructions

`node` and `npm` are required to build the extension.

Install dependencies with `npm install`, then execute `npm run build:<platform>`.

Valid platforms: `chrome`, `firefox`