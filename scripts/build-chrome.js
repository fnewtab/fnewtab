const ChromeExtension = require('crx');
const path = require('path');
const fs = require('fs-extra');

module.exports = async function(key) {
  var sourcePath = path.join(__dirname, '../src');
  var artifactsPath = path.join(__dirname, '../build/chrome');
  var chromeManifestPath = path.join(sourcePath, 'manifest-chrome.json');
  var manifestPath = path.join(sourcePath, 'manifest.json');
  try {
    var manifest = JSON.parse(await fs.readFile(chromeManifestPath, 'utf8'));
  } catch (err) {
    throw {
      message: 'Could not read manifest.',
      fsError: err
    };
  }
  try {
    await fs.copy(chromeManifestPath, manifestPath);
  } catch (err) {
    throw {
      message: 'Could not copy manifest.',
      fsError: err
    };
  }
  try {
    await fs.ensureDir(artifactsPath);
  } catch (err) {
    throw {
      message: 'Could not create or access the output directory.',
      fsError: err
    };
  }
  try {
    const crx = new ChromeExtension({
      privateKey: key
    });
    var loadedCrx = await crx.load(path.join(__dirname, '../src'))
    var crxBuffer = loadedCrx.pack();
  } catch (err) {
    try {
      await fs.unlink(manifestPath);
    } catch (err) {
      throw {
        message: 'Could not delete the temporary manifest.',
        fsError: err
      };
    }
    throw {
      message: 'Could not build the extension.',
      crxError: err
    };
  }
  var outputPath = path.join(artifactsPath, manifest.key + '-' + manifest.version + '.crx');
  try {
    await fs.writeFile(outputPath, crxBuffer);
  } catch (err) {
    try {
      await fs.unlink(manifestPath);
    } catch (err) {
      throw {
        message: 'Could not delete the temporary manifest.',
        fsError: err
      };
    }
    throw {
      message: 'Could not write the built extension.',
      fsError: err
    };
  }
  try {
    await fs.unlink(manifestPath);
  } catch (err) {
    throw {
      message: 'Could not delete the temporary manifest.',
      fsError: err
    };
  }
  return {
    extensionPath: outputPath
  };
}

if (!module.parent) {
  if (process.argv.length < 3) {
    console.error('Missing path to private key PEM file');
    return process.exit(1);
  }
  fs.readFile(process.argv[2], 'utf8').then((key) => {
    module.exports(key).then((result) => {
      console.log('Built successfully.');
      console.log(result);
    }).catch((err) => {
      console.error(err);
      return process.exit(2);
    });
  }).catch((err) => {
    console.error({
      message: 'Could not read the private key.',
      fsError: err
    });
    return process.exit(2);
  });;
}