const webExt = require('web-ext').default;
const path = require('path');
const fs = require('fs-extra');

module.exports = async function() {
  var sourcePath = path.join(__dirname, '../src');
  var artifactsPath = path.join(__dirname, '../build/firefox');
  var firefoxManifestPath = path.join(sourcePath, 'manifest-firefox.json');
  var manifestPath = path.join(sourcePath, 'manifest.json');
  try {
    await fs.copy(firefoxManifestPath, manifestPath);
  } catch (err) {
    throw {
      message: 'Could not copy manifest.',
      fsError: err
    };
  }
  try {
    await fs.ensureDir(artifactsPath);
  } catch (err) {
    throw {
      message: 'Could not create or access the output directory.',
      fsError: err
    };
  }
  try {
    var result = await webExt.cmd.build({
      sourceDir: sourcePath,
      artifactsDir: artifactsPath
    }, {
      shouldExitProgram: false,
    });
  } catch (err) {
    try {
      await fs.unlink(manifestPath);
    } catch (err) {
      throw {
        message: 'Could not delete the temporary manifest.',
        fsError: err
      };
    }
    throw {
      message: 'Could not build the extension.',
      webExtError: err
    };
  }
  try {
    await fs.unlink(manifestPath);
  } catch (err) {
    throw {
      message: 'Could not delete the temporary manifest.',
      fsError: err
    };
  }
  return result;
}

if (!module.parent) {
  module.exports().then((result) => {
    console.log('Built successfully.');
    console.log(result);
  }).catch((err) => {
    console.error(err);
    return process.exit(2);
  });
}