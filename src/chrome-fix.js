// chrome, why?

var isChrome = false;

if (browser == undefined && chrome != undefined) {
  var browser = chrome;
  isChrome = true;
}

function sendMessageWrapper(message) {
  if (isChrome) {
    return new Promise(function(resolve, reject) {
      browser.runtime.sendMessage(message, function(reply) {
        if (browser.runtime.lastError) {
          reject(browser.runtime.lastError);
        } else {
          resolve(reply);
        }
      });
    });
  } else {
    return browser.runtime.sendMessage(message);
  }
}

function storageWrapper(area, action, data) {
  if (isChrome) {
    return new Promise(function(resolve, reject) {
      browser.storage[area][action](data, function(data) {
        if (browser.runtime.lastError) {
          reject(browser.runtime.lastError);
        } else {
          resolve(data);
        }
      });
    });
  } else {
    return browser.storage[area][action](data);
  }
}