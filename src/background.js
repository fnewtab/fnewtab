var homepageData = {
  error: 'not_loaded'
};

var homepageIndex = undefined;

function loadData() {
  if (homepageIndex === null) {
    homepageData = {
      error: 'not_set'
    };
    sendMessageWrapper({
      homepageData: homepageData
    });
    return;
  }
  homepageData = {
    error: 'not_loaded'
  };
  sendMessageWrapper({
    homepageData: homepageData
  });
  var xhr = new XMLHttpRequest();
  xhr.open('GET', homepageIndex, true);
  xhr.responseType = 'text';
  xhr.onreadystatechange = function () {
    if (xhr.readyState == xhr.DONE) {
      if (xhr.status == 200) {
        try {
          homepageData = JSON.parse(xhr.responseText);
          if (!homepageData.version || homepageData.version != 1) {
            homepageData = {
              error: 'unsupported'
            }
          }
        } catch (ex) {
          homepageData = {
            error: 'invalid_json'
          }
        }
      } else {
        homepageData = {
          error: 'loading_failed'
        }
      }
      sendMessageWrapper({
        homepageData: homepageData
      });
    }
  };
  xhr.send(null);
  return xhr;
}

browser.runtime.onMessage.addListener(function(message, _sender, sendResponse) {
  if (message.getHomepageData) {
    sendResponse(homepageData);
  } else if (message.reloadHomepageData) {
    loadData();
    sendResponse(undefined);
  } else {
    sendResponse(undefined);
  }
});

storageWrapper('managed', 'get', {
  index: null
}).then(function(data) {
  if (data.index != null) {
    homepageIndex = data.index
    loadData();
  } else {
    homepageIndex = null;
    homepageData = {
      error: 'not_set'
    };
    sendMessageWrapper({
      homepageData: homepageData
    });
  }
}, function(_err) {
  homepageIndex = null;
  homepageData = {
    error: 'not_set'
  };
  sendMessageWrapper({
    homepageData: homepageData
  });
});