﻿var loadingOptions = {
  light: {
    background: 'rgba(255,255,255,0.8)',
    imageColor: '#202020'
  },
  dark: {
    background: 'rgba(50,50,50,0.8)',
    imageColor: '#efefef'
  }
}

var loaderVisible = {}

function filterHTML(html) {
  var jqhtml = $('<div>' + html + '</div>');
  jqhtml.find('script').remove();
  jqhtml.find('meta').remove();
  jqhtml.find('link').remove();
  var warned = false;
  jqhtml.find('a').each(function() {
    if (!this.href.startsWith('http://') && !this.href.startsWith('https://')) {
      this.href = 'javascript:void(0)';
      if (!warned) {
        console.warn('At least one <a> tag in the message links to a scheme different from http:// or https://. This/These tags have had their href attribute replaced with javascript:void(0).');
        warned = true;
      }
    }
  });
  return jqhtml.wrap('<div>').parent().html();
}

function loader(show) {
  if (!loaderVisible.global && show) {
    loaderVisible.global = true;
    if ($('body').hasClass('dark')) {
      $.LoadingOverlay('show', loadingOptions.dark);
    } else {
      $.LoadingOverlay('show', loadingOptions.light);
    }
  } else if (loaderVisible.global && !show) {
    loaderVisible.global = false;
    $.LoadingOverlay('hide');
  }
}

function loaderElement(selector, show) {
  if (!loaderVisible[selector] && show) {
    loaderVisible[selector] = true;
    if ($('body').hasClass('dark')) {
      $(selector).LoadingOverlay('show', loadingOptions.dark);
    } else {
      $(selector).LoadingOverlay('show', loadingOptions.light);
    }
  } else if (loaderVisible[selector] && !show) {
    delete loaderVisible[selector];
    $(selector).LoadingOverlay('hide');
  }
}

function renderHomepageData(homepageData) {
  if (homepageData.error) {
    if (homepageData.error == 'not_loaded') {
      loader(true);
    } else {
      loader(false);
      $('#error').show();
      loader(false);
      if (homepageData.error == 'invalid_json') {
        $('#errorText').text(browser.i18n.getMessage('invalidDataError'));
      } else if (homepageData.error == 'loading_failed') {
        $('#errorText').text(browser.i18n.getMessage('connectionError'));
      } else if (homepageData.error == 'unsupported') {
        $('#errorText').text(browser.i18n.getMessage('unsupportedError'));
      } else if (homepageData.error == 'not_set') {
        $('#errorText').text(browser.i18n.getMessage('indexNotSetError'));
      }
    }
  } else {
    loader(false);
    $('#error').hide();
  }
  if (homepageData.search) {
    $('#search').show();
  } else {
    $('#search').hide();
  }
  if (homepageData.links) {
    $('#links').show();
    $('#links').html('');
    try {
      for (var link of homepageData.links) {
        var img = document.createElement('img');
        $(img).attr('src', link.image);
        $(img).attr('alt', link.name);
        $(img).attr('title', link.name);
        $(img).addClass('link-image');
        var a = document.createElement('a');
        $(a).attr('href', link.url),
        $(a).append(img);
        $(a).on('click', function() {
          loader(true);
        })
        $('#links').append(a);
      }
    } catch (ex) {
      console.error('Invalid links.', ex);
      $('#links').hide();
    }
  } else {
    $('#links').hide();
  }
  if (homepageData.message) {
    $('#message').show();
    $('#message').html('');
    loaderElement('#message', true);
    var xhr = new XMLHttpRequest();
    xhr.open('GET', homepageData.message, true);
    xhr.responseType = 'text';
    xhr.onreadystatechange = function () {
      if (xhr.readyState == xhr.DONE) {
        loaderElement('#message', false);
        if (xhr.status == 200) {
          var html = '';
          try {
            var data = JSON.parse(xhr.responseText);
            if (data.html) {
              html = data.html;
            } else {
              throw 'No "html" ke in message JSON output';
            }
          } catch (ex) {
            html = xhr.responseText;
          }
          $('#message').html(filterHTML(html));
          $('#message a').on('click', function() {
            var target = $(this).attr('target');
            if ((!target || target == '_self' || target == '_top') && (this.href.startsWith('http://') || this.href.startsWith('https://'))) {
              loader(true);
            }
          });
        } else if (xhr.status != 404) {
          console.error('Failed to load message');
          $('#message').hide();
        }
      }
    };
    xhr.send(null);
  } else {
    $('#message').hide();
  }
}

browser.runtime.onMessage.addListener(function(message, _sender, _sendResponse) {
  if (message.homepageData) {
    renderHomepageData(message.homepageData);
  }
});

browser.storage.onChanged.addListener(function(changes, _areaName) {
  if (changes.dark) {
    if (changes.dark.newValue == true) {
      $('#dark')[0].checked = true;
      $('body').addClass('dark');
    } else {
      $('#dark')[0].checked = false;
      $('body').removeClass('dark');
    }
  }
});

$(function() {
  storageWrapper('local', 'get', [ 'dark' ]).then(function(data) {
    if (data.dark == true) {
      $('#dark')[0].checked = true;
      $('body').addClass('dark');
    } else {
      $('#dark')[0].checked = false;
    }
  
    $('#dark').on('change', function() {
      if ($('#dark')[0].checked) {
        $('body').addClass('dark');
      } else {
        $('body').removeClass('dark');
      }
      setData = {
        'dark': $('#dark')[0].checked
      };
      function success() {}
      function fail(error) {
        alert(error);
        console.error(error);
      }
      storageWrapper('local', 'set', setData).then(success, fail);
    });
  
    $('#search').on('submit', function(e) {
      e.preventDefault();
      loader(true);
      browser.search.get().then((engines) => {
        for (var engine of engines) {
          if (engine.isDefault) {
            browser.tabs.getCurrent().then((tab) => {
              browser.search.search({
                query: $('#searchBox').val(),
                engine: engine.name,
                tabId: tab.id
              });
            }, (err) => {
              console.error(err);
              alert(browser.i18n.getMessage('searchError'));
              loader(false);
            });
            break;
          }
        }
      }, (err) => {
        console.error(err);
        alert(browser.i18n.getMessage('searchError'));
        loader(false);
      });
      return false;
    });
  
    $('.reload').on('click', function() {
      loader(true);
      sendMessageWrapper({
        reloadHomepageData: true
      });
    });
  
    $('.reload').text(browser.i18n.getMessage('retryButton'));
    $('#searchBox').attr('placeholder', browser.i18n.getMessage('searchPrompt'));
    $('#searchBtn').val(browser.i18n.getMessage('searchButton'));
    $('#darkLabel').text(browser.i18n.getMessage('darkMode'));
  
    sendMessageWrapper({
      getHomepageData: true
    }).then(renderHomepageData);
  });
  
  document.title = browser.i18n.getMessage('newTabTitle');
});