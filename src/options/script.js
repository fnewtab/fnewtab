﻿function recievedHomepageData(homepageData) {
  document.getElementById('reload').disabled = (homepageData.error && homepageData.error == 'not_loaded');
}

browser.runtime.onMessage.addListener(function(message, _sender, _sendResponse) {
  if (message.homepageData) {
    recievedHomepageData(message.homepageData);
  }
});

browser.storage.onChanged.addListener(function(changes, _areaName) {
  if (changes.dark) {
    document.getElementById('dark').checked = changes.dark.newValue;
  }
});

function storageFail(error) {
  alert(error);
  console.error(error);
}

window.addEventListener('load', function() {
  storageWrapper('local', 'get', [ 'dark' ]).then(function(data) {
    document.getElementById('reload').addEventListener('click', function() {
      document.getElementById('reload').disabled = true;
      sendMessageWrapper({
        reloadHomepageData: true
      });
    });
  
    sendMessageWrapper({
      getHomepageData: true
    }).then(recievedHomepageData);
  
    if (data.dark == true) {
      document.getElementById('dark').checked = true;
    } else {
      document.getElementById('dark').checked = false;
    }
  
    document.getElementById('dark').addEventListener('change', function() {
      setData = {
        'dark': document.getElementById('dark').checked
      };
      storageWrapper('local', 'set', setData);
    });
  
    document.getElementById('darkContainer').style.display = '';
  });
  document.getElementById('reload').innerText = browser.i18n.getMessage('reloadButton');
  document.getElementById('darkLabel').innerText = browser.i18n.getMessage('darkMode');
  document.title = browser.i18n.getMessage('optionsTitle');
});